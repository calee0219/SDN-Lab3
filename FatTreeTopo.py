#!/usr/bin/env python
from mininet.topo import Topo
from mininet import net
from mininet.net import Mininet

POD_NUM = 4

class FatTreeTopo(Topo):
    """
    A Simple FatTree Topo
    """

    def create_pod(self, order):
        # Add host and switch
        ## Host
        h1 = self.addHost('h{}01'.format(order+1))
        h2 = self.addHost('h{}02'.format(order+1))
        h3 = self.addHost('h{}03'.format(order+1))
        h4 = self.addHost('h{}04'.format(order+1))
        ## Edge Switch
        e1 = self.addSwitch('s3{:03d}'.format(order*2+1))
        e2 = self.addSwitch('s3{:03d}'.format(order*2+2))
        ## Aggregation
        a1 = self.addSwitch('s2{:03d}'.format(order*2+1))
        a2 = self.addSwitch('s2{:03d}'.format(order*2+2))

        # Add links (100Mbps)
        ## Agg <-> Edge
        self.addLink(a1, e1, bw=100)
        self.addLink(a1, e2, bw=100)
        self.addLink(a2, e1, bw=100)
        self.addLink(a2, e2, bw=100)
        ## Edge <-> Host
        self.addLink(e1, h1, bw=100)
        self.addLink(e1, h2, bw=100)
        self.addLink(e2, h3, bw=100)
        self.addLink(e2, h4, bw=100)

        return {'h1': h1, 'h2': h2, 'h3': h3, 'h4': h4, \
                'e1': e1, 'e2': e2, 'a1': a1, 'a2': a2}

    def __init__(self):
        # Initialize topology
        Topo.__init__(self)

        # Create pod and core
        c = {} # core switch
        p = {} # pod
        for pod_ord in range(POD_NUM):
            p[pod_ord] = self.create_pod(pod_ord)
            c[pod_ord] = self.addSwitch('s1{:03d}'.format(pod_ord+1))

        # Link Core to pod
        for core_pod_ord in range(POD_NUM):
            if core_pod_ord < (POD_NUM / 2):
                for agg_pod_ord in range(POD_NUM):
                    self.addLink('s1{:03d}'.format(core_pod_ord+1), \
                                 's2{:03d}'.format(agg_pod_ord*2+1), \
                                 bw=1000, loss=2)
            else:
                for agg_pod_ord in range(POD_NUM):
                    self.addLink('s1{:03d}'.format(core_pod_ord+1), \
                                 's2{:03d}'.format(agg_pod_ord*2+2), \
                                 bw=1000, loss=2)

topos = {'fattree': (lambda: FatTreeTopo())}
